// Start bài 1
function sapXep() {
  var a = document.getElementById("so-thu-1").value * 1;
  var b = document.getElementById("so-thu-2").value * 1;
  var c = document.getElementById("so-thu-3").value * 1;
  var result;
  if (a > b) {
    if (b > c) {
      result = document.getElementById(
        "result"
      ).innerHTML = `Kết quả: ${c} < ${b} < ${a}`;
    } else if (a > c) {
      result = document.getElementById(
        "result"
      ).innerHTML = `Kết quả: ${b} < ${c} < ${a}`;
    } else {
      result = document.getElementById(
        "result"
      ).innerHTML = `Kết quả: ${b} < ${a} < ${c}`;
    }
  } else if (b > c) {
    if (a > c) {
      result = document.getElementById(
        "result"
      ).innerHTML = `Kết quả: ${c} < ${a} < ${b}`;
    } else {
      result = document.getElementById(
        "result"
      ).innerHTML = `Kết quả: ${a} < ${c} < ${b}`;
    }
  } else {
    result = document.getElementById(
      "result"
    ).innerHTML = `Kết quả: ${a} < ${b} < ${c}`;
  }
}
// End bài 1

// Start bài 2
function btnSelect() {
  var members = document.getElementById("select-member").value;
  var hello;

  if (members == "nguoila") {
    hello = "Xin chào Người lạ ơi!";
  } else if (members == "bo") {
    hello = "Xin chào Bố!";
  } else if (members == "me") {
    hello = "Xin chào Mẹ!";
  } else if (members == "anhtrai") {
    hello = "Xin chào Anh Trai!";
  } else if (members == "emgai") {
    hello = "Xin chào Em Gái!";
  }
  document.getElementById("Members").innerHTML = hello;
}
// End bài 2

// Start bài 3
function count() {
  var soThuNhat = document.getElementById("sothu-nhat").value * 1;
  var soThuHai = document.getElementById("sothu-hai").value * 1;
  var soThuBa = document.getElementById("sothu-ba").value * 1;

  var soLe = 0;
  var soChan = 0;

  soLe = (soThuNhat % 2) + (soThuHai % 2) + (soThuBa % 2);
  soChan = 3 - soLe;

  var result = `Có ${soLe} số lẻ, ${soChan} số chẵn`;

  document.getElementById("ketqua").innerHTML = result;
}
// End bài 3

// Start bài 4
function duDoan() {
  var canhA = document.getElementById("a").value * 1;
  var canhB = document.getElementById("b").value * 1;
  var canhC = document.getElementById("c").value * 1;

  var ketQua = "";

  if (canhA === canhB && canhA === canhC && canhC === canhA) {
    ketQua = "Hình tam giác đều";
  } else if (canhA === canhB || canhA === canhC || canhB === canhC) {
    ketQua = "Hình tam giác cân";
  } else if (
    canhA * canhA === canhB * canhB + canhC * canhC ||
    canhB * canhB === canhA * canhA + canhC * canhC ||
    canhC * canhC === canhA * canhA + canhB * canhB
  ) {
    ketQua = "Hình tam giác vuông";
  } else {
    ketQua = "Một loại tam giác nào đó";
  }
  document.getElementById("result4").innerHTML = ketQua;
}
// End bài 4

// Start bài 5
function ngayHomQua() {
  var ngay = Number(document.getElementById("ngay").value);
  var thang = Number(document.getElementById("thang").value);
  var nam = Number(document.getElementById("nam").value);

  var ngayHomNay = function () {
    switch (thang) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12: {
        days = 31;
        break;
      }
      case 4:
      case 6:
      case 9:
      case 11: {
        days = 30;
        break;
      }
      case 2: {
        days = 28;
        break;
      }
    }
    return days;
  };

  var ngayHomQua = "";
  var thangHomQua = "";
  var namHomQua = "";
  if (ngay > 31) {
    alert("Ngày không xác định!");
  } else if (thang > 12) {
    alert("Tháng không xác định!");
  } else if (ngay === 1 && thang === 1) {
    ngayHomQua = ngayHomNay();
    thangHomQua = 12;
    namHomQua = nam - 1;
  } else if (ngay === 1) {
    ngayHomQua = ngayHomNay();
    thangHomQua = thang - 1;
    namHomQua = nam;
  } else if (ngay !== 1) {
    ngayHomQua += ngay - 1;
    thangHomQua = thang;
    namHomQua = nam;
  }
  document.getElementById(
    "result5"
  ).innerHTML = `${ngayHomQua} / ${thangHomQua} / ${namHomQua}`;
}

function ngayMai() {
  var ngay = Number(document.getElementById("ngay").value);
  var thang = Number(document.getElementById("thang").value);
  var nam = Number(document.getElementById("nam").value);

  var ngayHomNay = function () {
    switch (thang) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12: {
        days = 31;
        break;
      }
      case 4:
      case 6:
      case 9:
      case 11: {
        days = 31;
        break;
      }
      case 2: {
        days = 28;
        break;
      }
      // default: {
      //   alert("Tháng không xác định!");
      //   break;
      // }
    }
    return days;
  };

  var ngayKeTiep = "";
  var thangKeTiep = "";
  var namKeTiep = "";
  if (ngay > 31) {
    alert("Ngày không xác định!");
  } else if (thang > 12) {
    alert("Tháng không xác định!");
  } else if (ngay === ngayHomNay() && thang === 12) {
    ngayKeTiep = 1;
    thangKeTiep = 1;
    namKeTiep = nam + 1;
  } else if (ngay === ngayHomNay()) {
    ngayKeTiep = 1;
    thangKeTiep = thang + 1;
    namKeTiep = nam;
  } else if (ngay !== ngayHomNay()) {
    ngayKeTiep += ngay + 1;
    thangKeTiep = thang;
    namKeTiep = nam;
  }

  document.getElementById(
    "result5"
  ).innerHTML = `${ngayKeTiep} / ${thangKeTiep} / ${namKeTiep}`;
}
// End bài 5

// Start bài 6
function tinhNgay() {
  var ngayThang = Number(document.getElementById("month").value);
  var ngayNam = Number(document.getElementById("year").value);
  var namNhuan = ngayNam % 4 === 0;

  var days = "";

  var lichNgay = function () {
    switch (ngayThang) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12: {
        days = 31;
        break;
      }
      case 4:
      case 6:
      case 9:
      case 11: {
        days = 30;
        break;
      }
      case 2: {
        days = 28;
        break;
      }
    }
    return days;
  };
  if (ngayThang > 12) {
    days = 0;
    alert("Tháng không xác định!");
  } else if (namNhuan && ngayThang === 2) {
    days = 29;
  } else {
    days = lichNgay();
  }
  document.getElementById(
    "result6"
  ).innerHTML = `Tháng ${ngayThang} năm ${ngayNam} có ${days} ngày`;
}
// End bài 6

// Start bài 7
function docSo() {
  const baChuSo = Number(document.getElementById("ba-chu-so").value);
  var hangTram = 0;
  var hangChuc = 0;
  var hangDonVi = 0;

  hangTram += Math.floor(baChuSo / 100);
  hangChuc += Math.floor((baChuSo % 100) / 10);
  hangDonVi += baChuSo % 10;

  var docLan1 = function (number) {
    switch (number) {
      case 1: {
        read = "một";
        break;
      }
      case 2: {
        read = "hai";
        break;
      }
      case 3: {
        read = "ba";
        break;
      }
      case 4: {
        read = "bốn";
        break;
      }
      case 5: {
        read = "năm";
        break;
      }
      case 6: {
        read = "sáu";
        break;
      }
      case 7: {
        read = "bảy";
        break;
      }
      case 8: {
        read = "tám";
        break;
      }
      case 9: {
        read = "chín";
        break;
      }
      case 0: {
        read = "";
        break;
      }
      default: {
        alert("Error Number");
      }
    }
    return read;
  };

  var docLan2 = function () {
    switch (hangDonVi) {
      case 1: {
        read = "mốt";
        break;
      }
      case 2: {
        read = "hai";
        break;
      }
      case 3: {
        read = "ba";
        break;
      }
      case 4: {
        read = "bốn";
        break;
      }
      case 5: {
        read = "năm";
        break;
      }
      case 6: {
        read = "sáu";
        break;
      }
      case 7: {
        read = "bảy";
        break;
      }
      case 8: {
        read = "tám";
        break;
      }
      case 9: {
        read = "chín";
        break;
      }
      case 0: {
        read = "";
        break;
      }
      default: {
        alert("Error");
      }
    }
    return read;
  };

  if (hangTram === 0 || hangChuc === 0 || hangTram > 10) {
    alert("Số hàng trăm không xác định được");
  } else if (hangChuc === 1 && hangDonVi === 1) {
    tram = docLan1(hangTram) + " trăm ";
    chuc = " mười ";
    donVi = " một ";
  } else if (hangChuc === 1) {
    tram = docLan1(hangTram) + " trăm ";
    chuc = " mười ";
    donVi = docLan2();
  } else {
    tram = docLan1(hangTram) + " trăm ";
    chuc = docLan1(hangChuc) + " mươi ";
    donVi = docLan2();
  }
  document.getElementById("result7").innerHTML = `${tram} ${chuc} ${donVi}`;
}
// End bài 7

// Start bài 8
function tim() {
  var x = +document.getElementById("x").value;
  var x1 = +document.getElementById("x1").value;
  var x2 = +document.getElementById("x2").value;
  var x3 = +document.getElementById("x3").value;
  var y = +document.getElementById("y").value;
  var y1 = +document.getElementById("y1").value;
  var y2 = +document.getElementById("y2").value;
  var y3 = +document.getElementById("y3").value;
  var sv1 = document.getElementById("sinh-vien-1").value;
  var sv2 = document.getElementById("sinh-vien-2").value;
  var sv3 = document.getElementById("sinh-vien-3").value;

  var d1 = 0;
  var d2 = 0;
  var d3 = 0;

  d1 += (x - x1) * (x - x1) + (y - y1) * (y - y1);
  console.log(d1);
  d2 += (x - x2) * (x - x2) + (y - y2) * (y - y2);
  console.log(d2);
  d3 += (x - x3) * (x - x3) + (y - y3) * (y - y3);
  console.log(d3);

  var result = "";

  if (d1 > d2 && d1 > d3) {
    result = sv1;
  } else if (d2 > d3 && d2 > d1) {
    result = sv2;
  } else if (d3 > d2 && d3 > d1) {
    result = sv3;
  }

  document.getElementById(
    "result8"
  ).innerHTML = `Sinh viên xa nhất trường: ${result}`;
}
// End bài 8
